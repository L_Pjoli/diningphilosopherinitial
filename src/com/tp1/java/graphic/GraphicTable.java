package com.tp1.java.graphic;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

@SuppressWarnings("serial")
public class GraphicTable extends Frame implements WindowListener {

	private static final int SIZEOFWINDOW = 200;
	private static final int PHILOSOPHERNUMBER = 5;
	private static final int OFFSET_Y = 70;
	private static final int PLATE_SIZE = 20;
	private static final int CHOPSTICK_SIZE = 40;

	private Point center;
	private GraphicPlate plates[];
	private GraphicChopstick chopsticks[];

	public GraphicTable() {
		super();

		addWindowListener(this);
		setTitle("Dining Philosophers");
		setSize(SIZEOFWINDOW, SIZEOFWINDOW);
		setBackground(Color.darkGray);

		center = new Point(getSize().width / 2, getSize().height / 2);
		plates = createPlates();
		chopsticks = createChopsticks();

		setVisible(true);
		setResizable(false);
	}

	public GraphicPlate[] createPlates() {
		GraphicPlate[] plates = new GraphicPlate[PHILOSOPHERNUMBER];
		for (int i = 0; i < PHILOSOPHERNUMBER; i++) {
			plates[i] = new GraphicPlate(i, center, 
						new Point(center.x, center.y - OFFSET_Y), PLATE_SIZE);
		}
		return plates;
	}

	public GraphicChopstick[] createChopsticks() {
		GraphicChopstick[] chopsticks = new GraphicChopstick[PHILOSOPHERNUMBER];
		for (int i = 0; i < PHILOSOPHERNUMBER; i++) {
			chopsticks[i] = new GraphicChopstick(i, center, 
							new Point(center.x, center.y - OFFSET_Y),
							new Point(center.x, center.y - CHOPSTICK_SIZE));
		}
		return chopsticks;
	}

	public void isHungry(int philosopherId) {
		plates[philosopherId].setColor(philosopherId);
		repaint();
	}

	public void isThinking(int philosopherId) {
		plates[philosopherId].setColor(-1);
		repaint();
	}

	public void takeChopstick(int philosopherId, int chopsticksId) {
		chopsticks[chopsticksId].setColor(philosopherId);
		repaint();
	}

	public void releaseChopstick(int chopsticksId) {
		chopsticks[chopsticksId].setColor(-1);
		repaint();
	}

	public void paint(Graphics graph) {
		for (int i = 0; i < PHILOSOPHERNUMBER; i++) {
			plates[i].draw(graph);
			chopsticks[i].draw(graph);
		}
	}

	/*
	 * Window events management from WindowListener implementation
	 */
	public void windowOpened(WindowEvent evt) { }
	public void windowClosing(WindowEvent evt) {System.exit(0);}
	public void windowClosed(WindowEvent evt) { }
	public void windowIconified(WindowEvent evt) { }
	public void windowDeiconified(WindowEvent evt) { }
	public void windowActivated(WindowEvent evt) { }
	public void windowDeactivated(WindowEvent evt) { }

}
