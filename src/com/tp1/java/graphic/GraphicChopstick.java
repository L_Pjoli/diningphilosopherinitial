package com.tp1.java.graphic;

import java.awt.*;

import com.tp1.java.service.Service;

class GraphicChopstick {
	private static final int CONST_ANGLE = 72;
	private static final int CONST_ANGLE_OFFSET = 36;
	private static final int COORD_OFFSET = 15;

	private Point coordStart;
	private Point coordEnd;
	private Color color;

	private int angle;
	private int colorId;

	public GraphicChopstick(int idPhilosopher, Point center, Point coordStart, Point coordEnd) {
		this.colorId = -1;
		setColor(colorId);

		this.angle = CONST_ANGLE * idPhilosopher + CONST_ANGLE_OFFSET;
		this.coordStart = new Point(Service.rotate(coordStart, center, angle));
		this.coordStart.y += COORD_OFFSET;

		this.coordEnd = new Point(Service.rotate(coordEnd, center, angle));
		this.coordEnd.y += COORD_OFFSET;
	}

	public void draw(Graphics g) {
		g.setColor(color);
		g.drawLine(coordStart.x, coordStart.y, coordEnd.x, coordEnd.y);
	}

	public void setColor(int colorId) {
		this.colorId = colorId;

		switch (colorId) {
		case -1:
			this.color = Color.black;
			break;
		case 0:
			this.color = Color.red;
			break;
		case 1:
			this.color = Color.blue;
			break;
		case 2:
			this.color = Color.green;
			break;
		case 3:
			this.color = Color.yellow;
			break;
		case 4:
			this.color = Color.white;
			break;
		}
	}

}
