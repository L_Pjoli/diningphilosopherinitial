package com.tp1.java.graphic;

import java.awt.*;

import com.tp1.java.service.Service;

class GraphicPlate {
	private static final int CONST_ANGLE = 72;
	private static final int OFFSET_X = 10;
	private static final int OFFSET_Y = 5;

	private Point coords;
	private Color color;
	private int colorId;
	private int angle;
	private int size;

	public GraphicPlate(int idPhilosopher, Point center, Point coords, int size) {
		this.size = size;

		this.colorId = -1;
		setColor(colorId);

		this.angle = CONST_ANGLE * idPhilosopher;
		this.coords = new Point(Service.rotate(coords, center, angle));
		this.coords.x -= OFFSET_X;
		this.coords.y += OFFSET_Y;
	}

	public void setColor(int colorId) {
		this.colorId = colorId;

		switch (colorId) {
		case -1:
			this.color = Color.black;
			break;
		case 0:
			this.color = Color.red;
			break;
		case 1:
			this.color = Color.blue;
			break;
		case 2:
			this.color = Color.green;
			break;
		case 3:
			this.color = Color.yellow;
			break;
		case 4:
			this.color = Color.white;
			break;
		}
	}

	public void draw(Graphics g) {
		g.setColor(color);
		g.fillOval(coords.x, coords.y, size, size);
	}

}
