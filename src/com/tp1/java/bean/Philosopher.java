package com.tp1.java.bean;

import java.util.logging.Level;

import com.tp1.java.app.DiningPhilosophers;
import com.tp1.java.graphic.GraphicTable;

public class Philosopher extends Thread {
	private GraphicTable table;
	private Chopstick left;
	private Chopstick right;
	private int id;

	private static final int TIMETHINK_MAX = 5000;
	private static final int TIMENEXTFORK = 100;
	private static final int TIMEEAT_MAX = 2000;

	public Philosopher(int id, GraphicTable table, Chopstick left, Chopstick right) {
		this.id = id;
		this.table = table;
		this.left = left;
		this.right = right;
		setName("Philosopher " + id);
	}

	public void run() {
		for (;;) {
			thinking(id);
			hungry(id);
			eating();
			fed(id);
		}
	}

	public void thinking(int id) {
		table.isThinking(id);
		System.out.println(getName() + " thinks");
		try {
			sleep((long) (Math.random() * TIMETHINK_MAX));
		} catch (InterruptedException e) {
			DiningPhilosophers.log.log(Level.WARNING, e.toString());
		}
		System.out.println(getName() + " finished thinking");
	}

	public void hungry(int id) {
		System.out.println(getName() + " is hungry");
		table.isHungry(id);

		wantLeftChopstick(id);
		wantRightChopstick(id);
	}

	public void wantLeftChopstick(int id) {
		System.out.println(getName() + " wants left chopstick");
		left.take();
		table.takeChopstick(id, left.getId());
		System.out.println(getName() + " got left chopstick");
	}

	public void wantRightChopstick(int id) {
		try {
			sleep(TIMENEXTFORK);
		} catch (InterruptedException e) {
			DiningPhilosophers.log.log(Level.WARNING, e.toString());
		}
		System.out.println(getName() + " wants right chopstick");
		right.take();
		table.takeChopstick(id, right.getId());
		System.out.println(getName() + " got right chopstick");
	}

	public void eating() {
		System.out.println(getName() + " eats");
		try {
			sleep((long) (Math.random() * TIMEEAT_MAX));
		} catch (InterruptedException e) {
			DiningPhilosophers.log.log(Level.WARNING, e.toString());
		}
		System.out.println(getName() + " finished eating");
	}

	public void fed(int id) {
		releaseLeft(id);
		releaseRight(id);
	}

	public void releaseLeft(int id) {
		table.releaseChopstick(left.getId());
		left.release();
		System.out.println(getName() + " released left chopstick");
	}

	public void releaseRight(int id) {
		table.releaseChopstick(right.getId());
		right.release();
		System.out.println(getName() + " released right chopstick");
	}
}
