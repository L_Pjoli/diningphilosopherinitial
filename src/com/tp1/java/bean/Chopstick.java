package com.tp1.java.bean;

import java.util.logging.Level;

import com.tp1.java.app.DiningPhilosophers;

public class Chopstick {
	private int id;
	private boolean free;

	public Chopstick(int id) {
		this.id = id;
		free = true;
	}

	public synchronized void take() {
		while (!free) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println("boom !");
				DiningPhilosophers.log.log(Level.WARNING, "boom !");
			}
		}
		free = false;

	}

	public synchronized void release() {
		notifyAll();
		free = true;

	}

	public int getId() {
		return (id);
	}
}
