package com.tp1.java.app;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.tp1.java.service.Service;

public class DiningPhilosophers {
	private static LogManager lgmngr = LogManager.getLogManager();
	public static Logger log = lgmngr.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static void main(String args[]) {
		Service.diner();
	}
}
