package com.tp1.java.service;

import java.awt.Point;

import com.tp1.java.bean.Chopstick;
import com.tp1.java.bean.Philosopher;
import com.tp1.java.graphic.GraphicTable;

public class Service {
	
	private final static double RADIAN = 180.0;
	
	public static void diner() {
		GraphicTable table = new GraphicTable();

		Chopstick c0 = new Chopstick(0);
		Chopstick c1 = new Chopstick(1);
		Chopstick c2 = new Chopstick(2);
		Chopstick c3 = new Chopstick(3);
		Chopstick c4 = new Chopstick(4);

		Philosopher p0 = new Philosopher(0, table, c0, c4);
		Philosopher p1 = new Philosopher(1, table, c1, c0);
		Philosopher p2 = new Philosopher(2, table, c2, c1);
		Philosopher p3 = new Philosopher(3, table, c3, c2);
		Philosopher p4 = new Philosopher(4, table, c4, c3);

		p0.start();
		p1.start();
		p2.start();
		p3.start();
		p4.start();
	}

	public static Point rotate(Point point, Point centerPoint, int angle) {
		Point p = new Point();
		double a = Math.PI / (RADIAN / angle);
		System.out.println(centerPoint.x + " " + centerPoint.y);

		p.x = (int) (point.x * Math.cos(a) - point.y * 
					 Math.sin(a) - Math.cos(a) * 
					 centerPoint.x + Math.sin(a) * 
					 centerPoint.y + centerPoint.x);

		p.y = (int) (point.x * Math.sin(a) + point.y *
					 Math.cos(a) - Math.sin(a) * 
					 centerPoint.x - Math.cos(a) * 
					 centerPoint.y + centerPoint.y);

		return (p);
	}

}
