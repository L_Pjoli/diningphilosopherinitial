package com.tp1.test;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tp1.java.service.Service;

class ServiceTest {
	int tempsDepart = 0;
	int tempsPris = 0;

	@BeforeEach
	void setUp() throws Exception {}

	@AfterEach
	void tearDown() throws Exception {}

	@Test
	void rotateTest() {
		Point point = new Point(0,0);
		Point point2 = new Point(0,0);
		assertTrue(Service.rotate(point, point2, 0).getX() == 0);
	}

	@Test
	void rotateTest2() {
		Point point = new Point(-5,-5);
		Point point2 = new Point(-5,-5);
		assertTrue(Service.rotate(point, point2, -5).getX() == -5);
	}

}
